import { scrollToArticle } from "./nav.js";

const chars = ['=', '·'];

export const initWaveEffect = (resized) => {
    const paragraphsParents = resized ? document.querySelectorAll('.js-description.waved, .js-homepage, .js-about') : document.querySelectorAll('.js-article.open .js-description:not(.waved), .js-homepage:not(.waved), .js-about:not(.waved)');
    insertWaveEffect(paragraphsParents);
}

//init about/homepage/open

//toggle article => rewave

//resize redo waved elements

export const insertWaveEffect = (paragraphsParents) => {

    paragraphsParents.forEach(paragraphsParent => {
        const paragraphs = paragraphsParent.querySelectorAll('p');

        paragraphs.forEach($p => {

            const pBBox = $p.getBoundingClientRect();
    
            insertCharsStart($p);
            const $pChar = makePChar(chars[Math.round(Math.random() * (chars.length - 1))]);
            $p.append($pChar);
    
            const charBBox = $pChar.getBoundingClientRect();
            
            const charIndex = Math.ceil((charBBox.left-pBBox.left) / charBBox.width);
            applySine($pChar, charIndex);
            const charPos =  charIndex * charBBox.width;
            const charMargin = charPos - (charBBox.left-pBBox.left);
            
    
            const nbCharsPerLine = Math.floor(pBBox.width / charBBox.width);
    
            if(charIndex > nbCharsPerLine) $pChar.remove();
            
            for(let i = charIndex+1; i < nbCharsPerLine; i++){
                const $newPChar = makePChar(chars[Math.round(Math.random() * (chars.length - 1))]);
                $p.append($newPChar);
                applySine($newPChar, i);
                
            }
            
            for(let i = 0; i < nbCharsPerLine; i++){
                const $newPChar = makePChar(chars[Math.round(Math.random() * (chars.length - 1))]);
                $p.append($newPChar);
                applySine($newPChar, i);
    
            }
            $pChar.style.marginLeft = charMargin+'px';
        });
        
        paragraphsParent.classList.add('waved')
    });
};

const makePChar = (char) => {
    const $pChar = document.createElement('span');
    $pChar.classList.add('js-pchar');
    $pChar.innerText = char;
    return $pChar;
}

const insertCharsStart = ($p) => {
    for(let i = 0; i < 6; i++){
        const $pChar = makePChar(chars[Math.round(Math.random() * (chars.length - 1))]);
        $p.prepend($pChar);
        applySine($pChar, i);
    }
   
}

const applySine = ($char, index) => {
    $char.style.top = Math.sin(index/Math.random()*10) * 5 + 'px';
}; 

const setResizeEvent = (() => {
    let waveTimer;

    window.addEventListener('resize', e => {
        document.querySelectorAll('.js-pchar').forEach($pChar => {
            $pChar.remove();
            if(waveTimer != null)
                clearTimeout(waveTimer);
            waveTimer = setTimeout(e => {
                initWaveEffect(true);
            }, 1000);
        });
        let articleOpen = document.querySelector('.js-article.open')
        if(articleOpen) scrollToArticle(articleOpen);
    });

})();
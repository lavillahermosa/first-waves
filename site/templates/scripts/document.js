import router from "./Router.js";
import { addURLSegment, removeURLSegment, scrollToArticle, addContentLink } from "./nav.js";
import { initVideo, stopPlayingVideos } from "./video.js";
import { initSplideGallery, initZoomImages } from "./imageZoom.js";
import { playSound } from "./audio.js";
import { insertWaveEffect } from "./typoEffect.js";

export const settingsCssTransition = ".5s ease all";
export const transitionDuration = 500;

export const addContentDocument = async(documentThumbnail, onload) => {

    let articleParent  =  documentThumbnail.closest(".js-article");
    let documentContainer =  documentThumbnail.closest(".js-article").querySelector('.js-documentContainer');
    let href =  documentThumbnail.getAttribute("data-docurl"); 
    let documentOpenedContainer = document.querySelector(".article--documentOpened");
    
    if(documentOpenedContainer  && documentOpenedContainer !== articleParent) removeDocumentOpened(documentOpenedContainer);

    const html = await router.getScreen(href, {sourceArticleId : articleParent.getAttribute('data-id')}).then(response => response.text());
    documentContainer.innerHTML = html;
    // if(articleParent.classList.contains('open')) articleParent.classList.remove('open');
    
    if(documentContainer.classList.contains('hidden')){  
        documentContainer.classList.remove('hidden');
        let baseSpace = 9;
        articleParent.style.transform = `translateX(${-articleParent.querySelector('.article__col').offsetWidth - (baseSpace * 2)}px)`;
        articleParent.classList.add('article--documentOpening');

        setTimeout((e) => {
            articleParent.style.transition = settingsCssTransition;
            articleParent.style.transform = `translateX(${0}px)`;
            setTimeout((e) => {
                articleParent.classList.remove('article--documentOpening');
                articleParent.classList.add('article--documentOpened');
            }, transitionDuration);
        }, transitionDuration);

        content.classList.add('content--documentOpened');
    }else{
        if(!onload) removeURLSegment(documentThumbnail.closest('.js-article'));
    }

    insertWaveEffect(documentContainer.querySelectorAll('.js-description:not(.waved)'))


    if(!onload) addURLSegment(documentThumbnail.closest('.js-article'), href, documentThumbnail.getAttribute('data-slug'));
    scrollToArticle(articleParent);

    initFunctionDocument(articleParent);
    articleParent.querySelector('.js-documentClose').addEventListener('click', removeDocumentClick);
    articleParent.style.transition = "unset";

    if(documentThumbnail.classList.contains('opening')) documentThumbnail.classList.remove('opening')
} 

export const clickDocumentThumbnail = (e) => {
    e.preventDefault();
    e.stopPropagation();
    let clicked = e.currentTarget;
    addContentDocument(clicked, false);
}

export const removeDocumentOpened = (article) => {
    article.querySelector(".js-documentContainer").classList.add("hidden");
    article.classList.remove("article--documentOpened");
    article.style.transition = "unset";
    removeURLSegment(article);
    if(content.classList.contains('content--documentOpened')) content.classList.remove('content--documentOpened')
}

export const removeDocumentClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    let clicked = e.currentTarget;
    let article = clicked.closest(".js-article");
    stopPlayingVideos();
    removeDocumentTransition(article);
}

export const removeDocumentTransition = (article) => {
    article.classList.add('article--documentOpening');
    article.classList.remove('article--documentOpened');
    article.style.transition = settingsCssTransition;
    article.style.transform = `translateX(${-article.offsetWidth/2}px)`;
    setTimeout((e) => {
        article.querySelector(".js-documentContainer").classList.add('hidden');
        article.style.transition = "unset";
        article.style.transform = `translateX(${0}px)`;
        article.classList.remove('article--documentOpening')
    }, transitionDuration);
    removeURLSegment(article);
    if(content.classList.contains('content--documentOpened')) content.classList.remove('content--documentOpened')
}

const initFunctionDocument = (articleParent) => {
    articleParent.querySelectorAll(".js-documentContainer .js-imageZoom").forEach(image => {image.addEventListener('click', initSplideGallery)})
    articleParent.querySelectorAll(".js-documentContainer .js-sound").forEach(sound => sound.addEventListener('click', playSound));
    articleParent.querySelectorAll(".js-documentContainer .js-player").forEach(videoCont => initVideo(videoCont));
    articleParent.querySelectorAll(".js-documentContainer .js-link").forEach(link => {link.addEventListener('click', addContentLink)});
}
import { addContentNav } from "./nav.js";

let menu = document.querySelector('#main-nav');
let activeFilters = [] ;
let activeFiltersYears = [];
let indexItems = document.querySelectorAll('.js-indexItem');
let filters = document.querySelectorAll('.js-filter');
export const filtersYears = document.querySelectorAll('.js-filterYear')
let hamburgers = document.querySelectorAll('.js-navButton');
let aboutBtn = document.querySelectorAll('.js-aboutBtn');

export const initMenu = () => {

    filters.forEach((filter, i) => {
        filter.addEventListener('click', toggleFilter)
    });

    indexItems.forEach(subcategory => {
        subcategory.addEventListener('mouseenter', highlightFilters);
        subcategory.addEventListener('mouseleave', removeHightlight)
        subcategory.addEventListener('click', addContentNav)
    });
    
    hamburgers.forEach(hamburger => {
        hamburger.addEventListener('click', toggleMenu);
    })

    aboutBtn.forEach(button => {
        button.addEventListener('click', toggleAbout);
    })

}

const range = (start, stop, step) => Array.from({ length: (stop - start) / step + 1 }, (_, i) => (start + i)* step);

export const highlightFilters = (e) => {
    let hovered = e.currentTarget;
    let hoveredClasses = [...hovered.classList];
    let rangeYears = getRangeYears(e.currentTarget);

    filters.forEach(filter => {
        if(!filter.classList.contains('active') && hoveredClasses.includes(filter.dataset.category)) filter.classList.add('hightlighted')
    });

    if(rangeYears.length > 0) filtersYears.forEach(filterYear => {
        if(!filterYear.classList.contains('active') && rangeYears.includes(parseInt(filterYear.dataset.year))) filterYear.classList.add('hightlighted');
    })
}

export const getRangeYears = (item) => {
    let yearBegin = parseInt(item.getAttribute('data-yearbegin'));
    let yearEnd = parseInt(item.getAttribute('data-yearend'));
    let rangeYears = 
    yearBegin && yearBegin !== 0 && yearEnd && yearEnd !== 0 && yearEnd > yearBegin ? range(yearBegin, yearEnd, 1) : range(yearBegin, yearBegin, 1);
    return rangeYears
}

export const removeHightlight = (e) => {
    document.querySelectorAll('.js-filter.hightlighted').forEach(filter => filter.classList.remove('hightlighted'))
}

export const toggleFilter = (e) => {
    e.preventDefault();
    let filter = e.currentTarget;
    filter.classList.contains('active') ? removeFilter(filter) : addFilter(filter);
}

export const addFilter = (filter) => {
    let isYearFilter = filter.classList.contains('js-filterYear') ? true : false ;
    filter.classList.add('active')
    let filterValue =  isYearFilter ? filter.getAttribute('data-year') : filter.getAttribute('data-category');
    isYearFilter ? activeFiltersYears.push(parseInt(filterValue)) : activeFilters.push(filterValue);
    updateIndex();
};

export const removeFilter = (filter) => {
    let isYearFilter = filter.classList.contains('js-filterYear') ? true : false ;
    filter.classList.remove('active');
    let filterValue =  isYearFilter ? filter.getAttribute('data-year') : filter.getAttribute('data-category');
    isYearFilter ? activeFiltersYears.splice(activeFilters.indexOf(parseInt(filterValue)), 1) : activeFilters.splice(activeFilters.indexOf(filterValue), 1);
    updateIndex();
};

let updateIndex = () => {
    indexItems.forEach(indexItem => {

        let yearBegin = parseInt(indexItem.getAttribute('data-yearbegin'));
        let yearEnd = parseInt(indexItem.getAttribute('data-yearend'));
        let rangeYears = 
        yearBegin && yearBegin !== 0 && yearEnd && yearEnd !== 0 && yearEnd > yearBegin ? range(yearBegin, yearEnd, 1) : range(yearBegin, yearBegin, 1);

        if(activeFilters.length === 0 && activeFiltersYears.length === 0 
        || activeFilters.length > 0 && activeFilters.some(r => indexItem.classList.contains(r)) 
        || activeFiltersYears.length > 0 && activeFiltersYears.some(element => rangeYears.includes(element))){
            indexItem.classList.remove('hidden')
        }else {
            indexItem.classList.add('hidden')
        }
    });
};

export const toggleMenu = (e) => {
    let content = document.querySelector('#content');
    menu.classList.remove('init');
    let subcategoryOpened = document.querySelector('.category__submenu.open')
    if(subcategoryOpened) subcategoryOpened.classList.remove('open')
    if(menu.classList.contains('open')){
        let activeFilters = document.querySelectorAll('.js-filter.active');
        activeFilters.forEach(filter => {
            filter.classList.remove('active');
            removeFilter(filter)
        })
    }else{
        let about = document.querySelector('.js-about');
        if(about.classList.contains('open')) toggleAbout(e);
    }
    menu.classList.toggle('open');
    content.classList.toggle('hidden');
}

export const toggleAbout = (e) => {
    e.preventDefault();

    let about = document.querySelector('.js-about');
    about.classList.remove('init');
    if(!about.classList.contains('open') && menu.classList.contains('open')) toggleMenu(e);
    about.classList.toggle('open')
}
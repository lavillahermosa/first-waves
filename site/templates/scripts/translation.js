export const initTranslations = () => {
    let buttons = document.querySelectorAll('.js-navLang');
    buttons.forEach(button => {
        button.addEventListener('click', switchLanguage)
    });
}

const switchLanguage = (e) => {
    e.preventDefault();
    let button = e.currentTarget;
    let url = window.location.pathname;
    const newURL = new URL(window.location.href);
    const currentParams = Object.fromEntries(newURL.searchParams.entries());
    currentParams['url'] = url;
    currentParams['langID'] = button.getAttribute('data-id');
    currentParams['query'] =  window.location.search;
    newURL.search = new URLSearchParams(currentParams).toString().replace(/%2C/g, ",");
    // history.pushState(false, false, newURL);
    window.location.href = newURL;

}
import router from "./Router.js";
import { initArticle, removeOpenedArticle, toggleArticle, updateHistory } from "./article.js";
import { stopPlayingVideos } from "./video.js";
import { toggleMenu } from "./menu.js";
import { insertWaveEffect } from "./typoEffect.js";
import { removeDocumentOpened, addContentDocument } from "./document.js";

let content = document.querySelector(".js-content");
export const filterKey = "items";

export const checkPopstate = () => {
    window.addEventListener("popstate", (e) => {
      const state = e.currentTarget.location;
      e.preventDefault();
      if (state !== null) {
        let href = state.pathname.split("/").pop();

        const currentURL = new URL(window.location.href);
        const currentParams = Object.fromEntries(currentURL.searchParams.entries());
        if (currentParams["articlesClosed"]) {
            removeOpenedArticle();
        }else{
            let opened = document.querySelector(`article[data-href="${state.pathname}"]`);
            if(opened) toggleArticle(opened, true)
        }

      } else {
        window.location.reload();
      }
    });
};

export const addContentNav = async(e) => {
    e.preventDefault();
    e.stopPropagation();
    stopPlayingVideos();

    let clicked = e.currentTarget;
    let clickedLink = clicked.querySelector('.index__item__link');
    let href = clickedLink.getAttribute('href');
    let id = clickedLink.parentNode.getAttribute("data-id");
    let sameArticleOpened = document.querySelector(`article[data-id="${id}"]`);

    if(id && sameArticleOpened){
        if(!sameArticleOpened.classList.contains('open')) toggleArticle(sameArticleOpened)
        toggleMenu();
        scrollToArticle(sameArticleOpened);
    }else{
        let documentOpenedContainer = document.querySelector(".article--documentOpened");
        if(documentOpenedContainer) removeDocumentOpened(documentOpenedContainer);
        removeOpenedArticle();
        if (!href) return false;
        toggleMenu();
        appendHtml(href, id);
    }
}

export const addContentLink = async(e) => {
    e.stopPropagation();
    e.preventDefault();
    stopPlayingVideos();

    let clickedLink = e.currentTarget.querySelector('a');
    let href = clickedLink.getAttribute('href');
    let id = clickedLink.getAttribute("data-id");
    let sameArticleOpened = document.querySelector(`article[data-id="${id}"]`);

    if(id && sameArticleOpened){
        let opened = document.querySelector('.js-article.open');
     
        if(opened !== sameArticleOpened){
            if(opened && opened.classList.contains("article--documentOpened")) removeDocumentOpened(opened);
            if(opened) opened.classList.remove("open")
        }
        if(!sameArticleOpened.classList.contains('open')) toggleArticle(sameArticleOpened);
        scrollToArticle(sameArticleOpened);

        //link is a document
        if(clickedLink.classList.contains('js-articleLinkDocument')){
            let documentThumbnail = sameArticleOpened.querySelector('.js-document[data-id="' + clickedLink.getAttribute('data-documentid') + '"]');
            if(documentThumbnail) addContentDocument(documentThumbnail, false)
        }
       
    }else{
        removeOpenedArticle();
        if (!href) return false;
        appendHtml(href, clickedLink.getAttribute("data-id"));
    }
}

export const addURLSegment = (article, href, documentName) => {
    if(!article || !href || !documentName) return false;
    let articleURL =  article.getAttribute("data-href");
    let newURLstring = articleURL + documentName;
    let newURLParams = addCurrParams(newURLstring);
    history.pushState(false, false, newURLParams);
}

export const removeURLSegment = (article) => {
    if(!article) return false;
    let articleURL =  article.getAttribute("data-href");
    let newURLParams = addCurrParams(articleURL);
    history.pushState(false, false, newURLParams);
}

export const addCurrParams = (href) => {
    const currentURL = new URL(window.location.href);
    const nextURL = new URL(window.location.origin + href);
    const currentParams = Object.fromEntries(currentURL.searchParams.entries());
    nextURL.search = new URLSearchParams(currentParams).toString().replace(/%2C/g, ",");
    return nextURL;
}

export const scrollToArticle = (article) => {
    content.scrollTo({
        top: article == article.parentNode.firstElementChild ? 0 : article.offsetTop,
        behavior: 'smooth'
    });
}

export const appendHtml = async(href, articleID) => {
    const html = await router.getScreen(href).then(response => response.text());
    content.insertAdjacentHTML("beforeend", html);
    let homepage = document.querySelector('.js-homepage');
    homepage ? homepage.remove() : scrollToArticle(content.querySelector('article:last-child'));
    initFunction(content.querySelector('article:last-child'));
    insertWaveEffect(content.querySelector('article:last-child').querySelectorAll('.js-description'));
    updatePageTitle(content.querySelector('article:last-child .article__title__col').innerText);
    updateHistory(addIDtoURL(href, articleID));
}

export const addIDtoURL = (href, id) => {
    const currentURL = new URL(window.location.href);
    const nextURL = new URL(window.location.origin + href);
    const currentParams = Object.fromEntries(currentURL.searchParams.entries());
    if (currentParams[filterKey]) {
        const currentValues = currentParams[filterKey].split(",");
        currentValues.push(id);
        currentParams[filterKey] = currentValues.join(",");
    } else {
        currentParams[filterKey] = id;
    }

    nextURL.search = new URLSearchParams(currentParams).toString().replace(/%2C/g, ",");
    return nextURL;
}

export const initHomepage = () => {
    let homepage = document.querySelector('.js-homepage');
    if(!homepage) return false;
    initArticle(homepage);
}

export const initFunction = (newArticle) => {
    newArticle ? initArticle(newArticle) : initHomepage();
}

export const updatePageTitle = (title) => {
    document.title = `=·=··first waves·${title}=·=··`;
}
import { initArticles } from "./article.js";
import { initAudios, initAudioPlayer } from "./audio.js";
import { initVideos } from "./video.js";
import { initMenu } from "./menu.js";
import { checkPopstate, scrollToArticle } from "./nav.js";
import { initWaveEffect } from "./typoEffect.js";
import { initHomepage } from "./nav.js";
import { initTranslations } from "./translation.js";

const Interface = (() => {
    const screens = {};

    const init = () => {
        initMenu();
        initArticles();
        initAudioPlayer();
        initAudios();
        initVideos();
        initWaveEffect();
        checkPopstate();
        initHomepage();
        initTranslations();
        let articleOpen = document.querySelector('.js-article.open')
        if(articleOpen) scrollToArticle(articleOpen);
    }

    return {
        init
    }

})();

export default Interface;
import { formatTime, displayDuration, calculateTime} from "./video.js";
const audioPlayer = document.querySelector('.js-audioPlayer');
const audio = audioPlayer.querySelector('audio');
const playButton = audioPlayer.querySelector('.js-audioPlay');
const playIcon = audioPlayer.querySelector('.js-playIcon');
const pauseIcon = audioPlayer.querySelector('.js-pauseIcon');
const removeIcon = audioPlayer.querySelector('.js-audioClose')

export const initAudios = () => {
    let sounds = document.querySelectorAll('.js-sound');
    sounds.forEach(sound => sound.addEventListener('click', playSound));
}

export const initAudioPlayer = () => {

    if (audio.readyState > 0) {
        displayDuration(audioPlayer, audio);
    }else{
        audio.addEventListener('loadedmetadata', () => {
            displayDuration(audioPlayer, audio);
        });
    }

    playButton.addEventListener("click", (e) => {
        if (audio.paused) {
            playAudio();
        } else {
            stopAudio();
        }
    });

    audioPlayer.querySelector(".js-playerProgressContainer").addEventListener("click", (e) => {
        let percentageBar = e.offsetX / e.target.offsetWidth;
        let percentageaudio = percentageBar * audio.duration;
        audio.currentTime = percentageaudio.toPrecision(3);
    });

    audioPlayer.querySelector(".js-playerProgress").addEventListener("click", (e) => {
        e.stopPropagation();
        const percentage = e.offsetX / audioPlayer.querySelector(".js-playerProgressContainer").offsetWidth;
        audio.currentTime = percentage * audio.duration;
    });
    
    audio.addEventListener("timeupdate", (e) => {
        const duration = audio.duration;
        const currentTime = audio.currentTime;
        audioPlayer.querySelector(".js-playerCurrent").innerHTML = formatTime(currentTime);
        const percentage = (currentTime / duration) * 100;
        audioPlayer.querySelector(".js-playerProgress").style.width = percentage.toPrecision(2) + "%";
    });

    removeIcon.addEventListener("click", removeAudioPlayer);
}

const playAudio = () => {
    audio.play();
    pauseIcon.classList.remove('hidden');
    playIcon.classList.add('hidden')
}

const stopAudio = () => {
    audio.pause();
    playIcon.classList.remove('hidden');
    pauseIcon.classList.add('hidden')
}

export const playSound = (e) => {
    let clicked = e.currentTarget;
    let src = clicked.getAttribute("data-src");
    audio.currentTime = 0;
    audio.setAttribute('src', src);
    audioPlayer.querySelector('.js-audioTitle').innerHTML = clicked.querySelector('.js-soundTitle').innerHTML;
    if (audio.readyState > 0) {
        displayDuration(audioPlayer, audio);
        playAudio()
    }else{
        audio.addEventListener('loadedmetadata', () => {
            displayDuration(audioPlayer, audio);
            playAudio();
        });
    }
    if(audioPlayer.classList.contains('hidden')) openAudioPlayer()
}

const openAudioPlayer = () => {
    audioPlayer.classList.remove('hidden');
    document.body.classList.add('body--audioPlaying')
}

const removeAudioPlayer = () => {
    audioPlayer.classList.add('hidden');
    document.body.classList.remove('body--audioPlaying');
    stopAudio();
    audio.currentTime = 0;
}

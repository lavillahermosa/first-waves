export const initVideos = () => {
    let videoConts = document.querySelectorAll('.js-player');
    videoConts.forEach(videoCont => {
        initVideo(videoCont)
    });
}

export const initVideo = (videoCont) => {
    let video = videoCont.classList.contains('audio') ? videoCont.querySelector('audio') : videoCont.querySelector('video');
    let playButton = videoCont.querySelector('.js-playerPlay');
    let playIcon = videoCont.querySelector('.js-playIcon');
    let pauseIcon = videoCont.querySelector('.js-pauseIcon');

    if (video.readyState > 0) {
        displayDuration(videoCont, video);
    }else{
        video.addEventListener('loadedmetadata', () => {
            displayDuration(videoCont, video);
        });
    }
    playButton.addEventListener("click", (e) => {
        if (video.paused) {
            video.play();
            pauseIcon.classList.remove('hidden');
            playIcon.classList.add('hidden')
        } else {
            video.pause();
            playIcon.classList.remove('hidden');
            pauseIcon.classList.add('hidden')
        }
    });

    videoCont.querySelector(".js-playerProgressContainer").addEventListener("click", (e) => {
        let percentageBar = e.offsetX / e.target.offsetWidth;
        let percentageVideo = percentageBar * video.duration;
        video.currentTime = percentageVideo.toPrecision(3);
    });

    videoCont.querySelector(".js-playerProgress").addEventListener("click", (e) => {
        e.stopPropagation();
        const percentage = e.offsetX / videoCont.querySelector(".js-playerProgressContainer").offsetWidth;
        video.currentTime = percentage * video.duration;
    });
    
    video.addEventListener("timeupdate", (e) => {
        const duration = video.duration;
        const currentTime = video.currentTime;
        videoCont.querySelector(".js-playerCurrent").innerHTML = formatTime(currentTime);
        const percentage = (currentTime / duration) * 100;
        videoCont.querySelector(".js-playerProgress").style.width = percentage.toPrecision(2) + "%";
    });
}


export const formatTime = (duration) => {
    const minutes = Math.floor(duration / 60);
    const seconds = Math.floor(duration % 60);
    return `${minutes < 10 ? "0" : ""}${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
};

export const displayDuration = (videoCont, video) => {
    let durationContainer = videoCont.querySelector('.js-playerTotal');
    if(durationContainer) durationContainer.innerHTML = calculateTime(video.duration);
}


export const calculateTime = (secs) => {
    const minutes = Math.floor(secs / 60);
    const seconds = Math.floor(secs % 60);
    const returnedSeconds = seconds < 10 ? `0${seconds}` : `${seconds}`;
    return `${minutes}:${returnedSeconds}`;
}

export const stopPlayingVideos = (e) => {
    let videoConts = document.querySelectorAll('.js-player video');
    videoConts.forEach(video => {
        video.pause();
    })
}

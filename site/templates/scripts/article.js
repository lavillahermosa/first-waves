import { filterKey, addCurrParams, scrollToArticle,  addContentLink, updatePageTitle } from "./nav.js";
import { removeDocumentTransition, addContentDocument, removeDocumentOpened } from "./document.js";
import { getRangeYears, filtersYears, addFilter, toggleMenu } from "./menu.js";
import { initSplideGallery, initZoomImages } from "./imageZoom.js";
import { clickDocumentThumbnail } from "./document.js";
import { stopPlayingVideos } from "./video.js";
import { insertWaveEffect } from "./typoEffect.js";

export const initArticles = () => {
    let articles = document.querySelectorAll(".js-article");
    articles.forEach(article => initArticle(article));
}

export const initArticle = (article) => {

    let buttonOpen = article.querySelectorAll(".js-articleHeader");
    let buttonRemove = article.querySelectorAll(".js-articleRemove");
    let buttonYear = article.querySelectorAll(".js-articleYear");
    let tags = article.querySelectorAll(".js-articleTag");
    let documents = article.querySelectorAll(".js-document");
    let links = article.querySelectorAll('.js-link');
    let images = article.querySelectorAll('.js-imageZoom');

    buttonOpen.forEach(header => {
        header.removeEventListener('click', toggleArticleClick);
        header.addEventListener('click', toggleArticleClick);
    })
    buttonRemove.forEach(buttonRemove => {
        buttonRemove.removeEventListener('click', removeArticle);
        buttonRemove.addEventListener('click', removeArticle);
    })
    buttonYear.forEach(buttonRemove => {
        buttonRemove.removeEventListener('click', toggleSamePeriod);
        buttonRemove.addEventListener('click', toggleSamePeriod);
    })
    tags.forEach(articleTag => {
        articleTag.removeEventListener('click', toggleFilterMenu)
        articleTag.addEventListener('click', toggleFilterMenu)
    })
    documents.forEach(document => {
        document.addEventListener('click', clickDocumentThumbnail);
        if(document.classList.contains("opening")) addContentDocument(document, true);
    });
    links.forEach(link => {
        link.addEventListener('click', addContentLink)
    });
    images.forEach(image => {
        image.addEventListener('click', initSplideGallery)
    })
}

export const toggleSamePeriod = (e) => {
    let clicked = e.currentTarget;
    let article = clicked.closest(".js-article");
    let articleId = article.getAttribute("data-id");
    let indexItem = document.querySelector(`.js-indexItem[data-id="${articleId}"]`);
    if(article && articleId && indexItem){
        let rangeYears = getRangeYears(indexItem);
        toggleMenu();
        if(rangeYears.length > 0) filtersYears.forEach(filterYear => {
            if(!filterYear.classList.contains('active') && rangeYears.includes(parseInt(filterYear.dataset.year))) addFilter(filterYear);
        })
    }
}

export const toggleFilterMenu = (e) => {
    let clicked = e.currentTarget;
    let clickedTag = clicked.getAttribute("data-category");
    let menuFilter = document.querySelector(`.js-filter[data-category="${clickedTag}"]`);
    if(menuFilter){
        toggleMenu();
        menuFilter.classList.add('active');
        addFilter(menuFilter)
    }
}

export const toggleArticleClick = (e) => {
    toggleArticle(e.currentTarget.closest(".js-article"));
}

export const toggleArticle = (article, popstate) => {
    stopPlayingVideos();
    
    if(article.classList.contains("open")){
        if(article.classList.contains("article--documentOpened")){
            removeDocumentTransition(article)
        }else{
            article.classList.remove("open");
            if(!popstate) updateHistory(window.location.href);
            updatePageTitle('');
        }
    }else{
        removeOpenedArticle();
        article.classList.add("open");
        updatePageTitle(article.querySelector('.article__title__col').innerText);
        scrollToArticle(article);
        if(!popstate) updateHistory(addCurrParams(article.getAttribute("data-href")))
    }

    insertWaveEffect(article.querySelectorAll('.js-description:not(.waved)'))
}

export const changeLanguagesBtn = (article) => {
    let btnEN = document.querySelector(`.js-navLang[data-id="1020"]`);
    let btnFR = document.querySelector(`.js-navLang[data-id="1122"]`);
    if(btnEN) btnEN.setAttribute("href", addCurrParams(article.getAttribute("data-hrefEN")));
    if(btnFR) btnFR.setAttribute("href", addCurrParams(article.getAttribute("data-hrefFR")))
}

export const removeArticle = (e) => {
    e.preventDefault();
    e.stopPropagation();
    let article = e.currentTarget.closest(".js-article");
    let id = article.getAttribute("data-id");
    const currentURL = new URL(window.location.href);
    article.remove();
    // removeArticleLanguagesBtn(id);
    updateHistory(removeIDinURL(currentURL, id))
}

export const removeArticleLanguagesBtn = (id) => {
    let languagesBtn = document.querySelectorAll('.js-navLang')
    languagesBtn.forEach(languageBtn => {
        const currentURL = new URL(languageBtn.getAttribute('href'));
        languageBtn.setAttribute("href", removeIDinURL(currentURL, id))
    });
}

export const removeIDinURL = (currentURL, id) => {
    const currentParams = Object.fromEntries(currentURL.searchParams.entries());
    if (currentParams[filterKey]) {
        const currentValues = currentParams[filterKey].split(",");
        if (!currentValues.includes(id)) return;
        currentValues.splice(currentValues.indexOf(id), 1);
        if (currentValues.length === 0) {
            delete currentParams[filterKey];
        } else {
            currentParams[filterKey] = currentValues.join(",");
        }
    }
    currentURL.search = new URLSearchParams(currentParams).toString().replace(/%2C/g, ",");
    return currentURL;
}

export const removeOpenedArticle = () => {
    let opened = document.querySelector('.js-article.open');
    if(opened && opened.classList.contains("article--documentOpened")) removeDocumentOpened(opened);
    if(opened) opened.classList.remove("open")
}

export const updateHistory = (url) => {
    const currentURL = new URL(url);
    const currentParams = Object.fromEntries(currentURL.searchParams.entries());
    const content = document.querySelector('.js-content');
    if(document.querySelectorAll('article').length == 0){
        window.location.href = currentURL['origin'];
    }else if(document.querySelectorAll('article.open').length == 0){
        currentParams["articlesClosed"] = true;
        content.classList.add('articlesClosed')
    }else{
        if(currentParams["articlesClosed"]) delete currentParams["articlesClosed"];
        if(content.classList.contains('articlesClosed')) content.classList.remove('articlesClosed');
    }   
    currentURL.search = new URLSearchParams(currentParams).toString().replace(/%2C/g, ",");
    history.pushState(false, false, currentURL);
}

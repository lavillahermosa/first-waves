import Splide from './splide.esm.js';
const carousel = document.querySelector('.js-carousel');
const carouselImages = document.querySelector('.js-splideImages');
const carouselCaption = document.querySelector('.js-carouselCaption');
const carouselClose = document.querySelector('.js-carouselClose');

export const initZoomImages = () => {
    let images = document.querySelectorAll('.js-imageZoom');
    images.forEach(image => {
        image.addEventListener('click', initSplideGallery)
    })
}

export const initSplideGallery = (e) => {
    let clicked = e.currentTarget;
    let parent = clicked.parentNode;
    let galleryItems = parent.querySelectorAll('.js-imageZoom');
    let imageIndex = Array.prototype.indexOf.call(galleryItems, clicked);
    let splide = "";

    galleryItems.forEach(galleryItem => {

        if(imageIndex == 0 && galleryItem == clicked) carouselCaption.innerHTML = clicked.querySelector('img').getAttribute('alt');
        let image = galleryItem.querySelector('img')
        let li = document.createElement('li');
        let img = document.createElement('img');

        img.src = image.getAttribute("data-srcbig") && image.getAttribute("data-srcbig") != "" ? image.getAttribute("data-srcbig") : image.src;
        img.alt = image.alt;
        li.setAttribute('class','splide__slide js-splideSlide');

        li.appendChild(img);
        carouselImages.appendChild(li);
    });

    if(carousel.classList.contains('hidden')) carousel.classList.remove('hidden')

    if(!carousel.classList.contains('is-active')){
        splide = new Splide( '.js-splide', {
            // arrows: false,
            pagination: false,
            padding: '12.5dvw',
            loop: true,
        } );
    
        splide.mount();
    
        splide.on( 'moved', function (active) {
            let caption =  document.querySelectorAll('.js-splideSlide')[active].querySelector('img').getAttribute('alt');
            if (caption){
                if(carouselCaption.classList.contains('hidden')) carouselCaption.classList.remove('hidden')
                carouselCaption.innerHTML = caption
            }else{
                carouselCaption.classList.add('hidden')
            }
        });

        carouselClose.addEventListener('click', closeCarousel);
    }

    splide.go(imageIndex);
}

const carouselCursor = (e) => {
    const percentage = carouselPosition(e);
    if (percentage < 0.5) {
        carousel.classList.remove("right");
        carousel.classList.add("left");
    } else {
        carousel.classList.remove("left");
        carousel.classList.add("right");
    }
}

const carouselClick = (e) => {
    const percentage = carouselPosition(e);
    if (percentage < 0.5) {
        prevSlide(e);
    } else {
        nextSlide(e);
    }
}

const prevSlide = (e) => {
    let active = document.querySelector('.js-imageZoom.zoomed');
    let nextImg = active.previousElementSibling;
    if(!nextImg){
        nextImg = active.parentNode.querySelector('.js-imageZoom:last-child')
    }
    active.classList.remove('zoomed');
    nextImg.classList.add('zoomed');
    setImage(nextImg);
}

const nextSlide = (e) => { 
    let active = document.querySelector('.js-imageZoom.zoomed');
    let nextImg = active.nextElementSibling;
    if(!nextImg){
        nextImg = active.parentNode.querySelector('.js-imageZoom:first-child')
    }
    active.classList.remove('zoomed');
    nextImg.classList.add('zoomed');
    setImage(nextImg);
}

const closeCarousel = (e) => {
    e.preventDefault();
    e.stopPropagation();
    carousel.classList.add("hidden");
    carousel.querySelector(".js-splideImages").innerHTML = "";
}

const carouselPosition = (e) => {
    const { left, width } = e.target.getBoundingClientRect();
    return (e.clientX - left) / width;
};
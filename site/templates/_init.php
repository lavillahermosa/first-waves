<?php namespace ProcessWire;

// Optional initialization file, called before rendering any template file.
// This is defined by $config->prependTemplateFile in /site/config.php.
// Use this to define shared variables, functions, classes, includes, etc. 

// chopper url
// chopper langue
// get l'article translation url + add parameters
// header php vers cette adresse

if($input->get('url') != null && $input->get('langID') != null){
    $targetLanguage = $languages->get($input->get('langID'));
    
    $targetPage = $pages->get($input->get('url'));
    if($targetPage->id == 0){
        $urlParts = explode('/', $input->get('url'));
        for($i = count($urlParts) - 1; $i >= 0; $i--){
            if($urlParts[$i] != ''){
                $targetPages = $pages->find('name='.$urlParts[$i]);
                unset($urlParts[$i]);
                if($targetPages->count() > 0){
                    $targetPageName = $targetPages->first->localName($targetLanguage);
                    if($targetPageName == '') $targetPageName = $targetPages->first->name;
                    break;
                }

            }
        }

        $newURL = implode('/', $urlParts);
        $targetPage = $pages->get($newURL);
    }

    if($targetPage->id == 0){
        $session->redirect('/');
    }

    $targetURL = $targetPage->localUrl($targetLanguage).((isset($targetPageName))?$targetPageName:'');

    if($input->get('query') != null){
        $targetURL .= $input->get('query');
    }
    
    $session->redirect('https://'.$config->httpHost.$targetURL);
}

?>
<?php
namespace ProcessWire;
require_once('_functions.php');

$documents = getLinkedNodes($page, ["chemins-node-document", "chemins-node-doc-story"]);
?>

<?php if(!$config->ajax): ?>
<main id="content" class="js-content">
<?php endif; ?>

    <article class="screen homepage open js-homepage">
        <div class="homepage__description">     
            <?= $page->text ?>
            <?php if($page->images && $page->images->count > 0):?>
                <div class="article__description__gallery">
                    <?php include('fragments/_gallery.php'); ?>
                </div>
            <?php endif ?>
        </div>
        <div class="homepage__documents">
            <ul class="homepage__documents__inner">
                <?php foreach($documents as $document):?>
                    <li class="homepage__document js-link"> 
                        <?php $documentParent = getLinkedNodes($document, [], [], ['chemins-edge-link']) ?>     
                        <a href="<?= $documentParent[0]->url() . $document->name ?>" data-id="<?=  $documentParent[0]->id ?>">
                            <?php if($document->images->count() > 0 || $document->videos->count() > 0 && $document->videos->first->thumbnail): 
                                    $image = $document->images->count() > 0?$document->images->first:$document->videos->first->thumbnail;
                            ?>
                                    <figure class="document"> 
                                        <img class="lazyload" data-src="<?= $image->url ?>" alt="<?= $image->description ?>">
                                        <figcaption><?= $document->title ?></figcaption>
                                    </figure>
                            <?php elseif($document->files && $document->files->count() > 0): ?>
                                <?php foreach ($document->pdfs as $pdf): ?>
                                    <?php $image = $pdf->toImage();?>
                                    <figure class="document">
                                        <img class="lazyload" data-src="<?= $image->size(1000, 0)->url ?>" alt="<?= $image->description ?>">
                                        <figcaption><?= $document->title ?></figcaption>
                                    </figure>
                                <?php endforeach ?>
                            <?php else: ?>
                                <div class="document document--thumbnail">
                                    <h3><?= $document->title ?></h3>
                                </div>
                            <?php endif ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </article>

<?php if(!$config->ajax):?>
</main>
<?php else:return $this->halt(); endif; ?>

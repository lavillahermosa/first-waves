<?php
namespace Processwire;

    //getLinkedNodes: get the linked nodes from a node as parameter
    function getLinkedNodes($node, $templates = [], $excludeTemplates = [], $linkTemplates = []){
        $linkedEdges = $node->references();
        $linkedNodes = new PageArray();
        
        foreach($linkedEdges as $linkedEdge){

            if(!empty($linkTemplates) && !in_array($linkedEdge->template, $linkTemplates)) continue;

            if($linkedEdge->chemins_node_a != $node){
                $linkedNode = $linkedEdge->chemins_node_a;
                
            }else{
                $linkedNode = $linkedEdge->chemins_node_b;
            
            }
    
            if($linkedNode !=null && in_array($linkedNode->template, $templates) || count($templates) == 0){
                $linkedNodes->add($linkedNode);
            }
        }

        return $linkedNodes;

    }

    function getYearString($article){
        $string = "";
        $yearBegin = $datetime->date('Y', $article->getUnformatted('chemins_date_begin'));
        $yearEnd = $datetime->date('Y', $article->getUnformatted('chemins_date_end')); 
        
        if($article->chemins_date_begin && $article->chemins_date_end && $yearBegin !== $yearEnd):
            $string = $yearBegin . "-" . $yearEnd;
        elseif($article->chemins_date_begin):
            $string = $yearBegin;
        elseif($article->chemins_date_end): 
            $string = $yearEnd;
        endif;

        return $string;
    }

?>
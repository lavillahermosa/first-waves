<?php namespace ProcessWire;

// Optional main output file, called after rendering page’s template file. 
// This is defined by $config->appendTemplateFile in /site/config.php, and
// is typically used to define and output markup common among most pages.
// 	
// When the Markup Regions feature is used, template files can prepend, append,
// replace or delete any element defined here that has an "id" attribute. 
// https://processwire.com/docs/front-end/output/markup-regions/
	
/** @var Page $page */
/** @var Pages $pages */
/** @var Config $config */
	
$home = $pages->get('/'); /** @var HomePage $home */

?><!DOCTYPE html>
<html lang="fr">
	<head id="html-head">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
		<title>=·=··first waves<?= $page->id != 1227?'·'.$page->title:''; ?>=·=··</title>
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>styles/global.css" />
		
		<script src="<?php echo $config->urls->templates; ?>scripts/main.js" type="module" defer></script>
		<script src="<?php echo $config->urls->templates; ?>scripts/lazysizes.min.js" async=""></script>
		<!-- Matomo -->
		<script>
		var _paq = window._paq = window._paq || [];
		/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function() {
			var u="//analytics.lvh0.com/";
			_paq.push(['setTrackerUrl', u+'matomo.php']);
			_paq.push(['setSiteId', '8']);
			var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
			g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
		})();
		</script>
		<!-- End Matomo Code -->
	</head>
	<body id="html-body">
		<?php include('fragments/_navigation.php'); ?>
		
		<div class="audio__player hidden js-audioPlayer">
			<audio title="" preload=”metadata”>
				<source src="" type="audio/mp3">
			</audio>  

			<div class="audio__inner">
				<div class="audio__line">
					<div class="audio__line__col">
						<button class="audio__play js-audioPlay">
							<svg class="audio__play__icon js-playIcon" viewBox="0 0 20 30" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M0 13V24L20 13L0 2V13Z" fill="black"/>
							</svg>
							<svg class="audio__play__icon hidden js-pauseIcon" viewBox="0 0 16 30" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M0 2V24H6V2H0Z" fill="black"/>
								<path d="M10 2V24H16V2H10Z" fill="black"/>
							</svg>
						</button>

						<div class="audio__title js-audioTitle">
							Kaleidoscope Amid Chakir
						</div>

						<div class="audio__times">
							<div class="audio__time js-playerCurrent">00:00</div>
							<div class="audio__time js-playerTotal">00:00</div>
						</div>
					</div>
					<div class="audio__line__col">
						<div class="audio__close js-audioClose">
							<?= __('Close', $tr) ?>
						</div>
					</div>
				</div>

				<div class="audio__line">
					<div class="player__progress__container js-playerProgressContainer">
						<div class="player__progress js-playerProgress"></div>
					</div>
				</div>
			</div>
		</div>

		<div id="content" class="">
			Default content
		</div>

		<div class="carousel js-carousel hidden">
			<div class="splide js-splide" role="group" aria-label="Splide Basic HTML Example">
				<div class="splide__track js-splideTrack">
					<ul class="splide__list js-splideImages">
					</ul>
				</div>
			</div>
			<div class="carousel__caption js-carouselCaption">
				<?= "hello world" ?>
			</div>
			<div class="carousel__close js-carouselClose">
				<?= __('Close', $tr) ?>
			</div>
		</div>
		
	
	</body>
</html>
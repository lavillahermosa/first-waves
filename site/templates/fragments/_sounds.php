<?php
namespace ProcessWire;
?>

<div class="article__col__group">
    <h3 class="article__subtitle">
        <p><?= __("Document sound(s)", $tr) ?></p>
    </h3>
    <div class="article__sounds">
        <?php foreach($page->sound_files as $sound): ?>   
        <?php $title = $sound->description ? $sound->description : $sound->name; ?>              
        <div class="article__sound js-sound" data-src="<?= $sound->url ?>">
            <div class="sound__title__container">
                <button class="sound__play">
                    <svg class="sound__play__icon js-playIcon" viewBox="0 0 20 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 13V24L20 13L0 2V13Z" fill="black"/>
                    </svg>
                </button>
                <div class="sound__title js-soundTitle">
                    <p><?= $title ?></p>
                </div>
            </div>
        </div>
        <?php endforeach ?>
    </div>
</div>
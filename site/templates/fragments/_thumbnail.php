<figure class="thumbnail">
    <div class="thumbnail__image">
        <div class="thumbnail__image__inner">
            <img class="lazyload<?= $image->ratio() < 1 ? " document__image--portrait" : "" ?>" data-src="<?= $image->size(400, 0)->url ?>" alt="<?= $image->description ? $image->description : $image->basename() ?>">
        </div>
    </div>
    <div class="thumbnail__text">
        <figcaption><?= $document->title ?></figcaption>
    </div>
</figure>
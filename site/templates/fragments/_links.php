<?php
namespace ProcessWire; 

if($page->template == "chemins-node-doc-story") $linkedNodes->add($pages->find("template=chemins-node-doc-story"));
if($linkedNodes->count > 0 && $linkedNodes->first->id !== 0):?>
<div class="article__col__group">
    <!-- <h3 class="article__subtitle">
        <p><?= __("Linked elements", $tr) ?></p>
    </h3> -->
    <ul class="article__links"> 
        <?php foreach($linkedNodes as $linkedNode): ?>
            <?php if($linkedNode && $linkedNode->title && $linkedNode->name !== "first-waves"): ?>
            <?php include('fragments/_link.php'); ?>
            <?php endif ?>
        <?php endforeach ?>
    </ul>
</div>

<?php endif ?>
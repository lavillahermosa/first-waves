<?php 
namespace ProcessWire; 

    $parent= getLinkedNodes($linkedNode, [], [], ['chemins-edge-link'])->first;   
    $href = "";
    $document = $linkedNode->template == "chemins-node-document" || $linkedNode->template == "chemins-node-doc-story" ? true : false;
    $documentParent = "";
    if($document){
        $documentParent = getLinkedNodes($linkedNode, [], [], ['chemins-edge-link']);
        $href = $documentParent[0]->url() . $linkedNode->name;
        $id = $documentParent[0]->id;
    }else{
        $href = $linkedNode->url;
        $id = $linkedNode->id;
    }

?>

<li class="article__link__container js-link<?= $document ? " article__link__container--document" : "" ?>">
    <?php if($document): ?>
        <a href="<?= $href ?>" class="article__link js-articleLinkDocument" data-id="<?= $id ?>" data-documentid = "<?= $linkedNode->id ?>">
    <?php else : ?>
        <a href="<?= $href ?>" class="article__link" data-id="<?= $id ?>">
    <?php endif ?>
        <?php if($parent &&  $parent->title && $parent->name !== "first-waves"): ?>
            <div class="article__link__type">
                <?= $parent->title ?>
            </div>
        <?php endif ?>
        <?= $linkedNode->title ?>
    </a>
</li>
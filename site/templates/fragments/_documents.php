<?php
namespace Processwire;
$thumbnailSize = 400;
if($documents->count() > 0):?>

<div class="article__col__group">
    <!-- <h3 class="article__subtitle">
        <p>Linked documents</p>
    </h3> -->
    <div class="documents">
        <?php foreach($documents as $document):?>
            <a href="<?= $page->url.$document->name ?>" data-docurl="<?= $document->url() ?>" 
            class="thumbnail__container js-document<?= !$isStacked && $input->urlSegment() && $input->urlSegment() == $document->name ? " opening" : "" ?>" 
            data-category="<?= getLinkedNodes($page, ["chemins-node-category"])->first->name ?>"
            data-slug="<?= $document->name ?>"
            data-id="<?= $document->id ?>"
            >
                <?php if($document->images->count() > 0): ?>
                        <?php $image = $document->images->first() ?>
                        <?php include('fragments/_thumbnail.php'); ?>
                <?php elseif($document->videos && $document->videos->count > 0 && $document->videos->first->thumbnail): ?>
                        <?php $image = $document->videos->first->thumbnail ?>
                        <?php include('fragments/_thumbnail.php'); ?>
                <?php elseif($document->pdfs && $document->pdfs->count() > 0): ?>
                        <?php $image = $document->pdfs->first->toImage() ?>
                        <?php include('fragments/_thumbnail.php'); ?>                  
                <?php else: ?>
                    <!-- sounds thumbnail -->
                    <?php if($document->sounds->find("thumbnail!=")->count() > 0) :?>
                        <?php $image = $document->sounds->find("thumbnail!=")->first->thumbnail ?>
                        <?php include('fragments/_thumbnail.php'); ?>
                    <!-- videos thumbnail -->
                    <?php elseif($document->videos->find("thumbnail!=")->count() > 0) :?>
                        <?php $image = $document->videos->find("thumbnail!=")->first->thumbnail ?>
                        <?php include('fragments/_thumbnail.php'); ?>
                    <?php else: ?>
                    <!-- text thumbnail -->
                        <figure class="thumbnail thumbnail--noimage">
                            <div class="thumbnail__image">
                                <div class="thumbnail__image__inner">
                                    <div class="thumbnail__image__type">
                                        <?php 
                                        $filesType = [];
                                        if($document->images && $document->images->count > 0) $filesType[] = __("Image", $tr);
                                        if($document->videos && $document->videos->count > 0) $filesType[] = __("Video", $tr);
                                        if($document->sound_files && $document->sound_files->count > 0) $filesType[] = __("Sound", $tr);
                                        if($document->pdfs && $document->pdfs->count() > 0) $filesType[] = "pdf";
                                        if($document->text && strlen($document->text) > 0) $filesType[] = __("Text", $tr);
                                        if(!empty($filesType)) echo " (" . implode(", ", $filesType ) . ")";
                                        ?>
                                    </div>
                                </figcaption>
                                </div>
                            </div>
                            <div class="thumbnail__text">
                                <figcaption><?= $document->title ?>
                            </div>
                        </figure>
                    <?php endif ?>
                <?php endif ?>
            </a>
        <?php endforeach; ?>
    </div>
</div>
<?php endif ?>
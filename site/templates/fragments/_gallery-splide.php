<?php namespace Processwire;

if($page->images->count > 1) : ?>
<div class="splide gallery">
    <div class="splide__track">
        <ul class="splide__list">
            <?php foreach($page->images as $image):?>
                <li class="splide__slide"><img src="<?= $image->size(1000, 0)->url ?>" alt="<?= $image->description ?>"></li>
            <?php endforeach?>
        </ul>
    </div>
    <div class="splide__btns">
        <div class="splide__btn splide__btn--prev">
            <?= __("Previous", $tr) ?>
        </div>
        <div class="splide__btn splide__btn--next">
            <?= __("Next", $tr) ?>
        </div>
        <div class="splide__index">
            <div class="splide__index__counter"></div>
        </div>
    </div>
    
</div>
<?php else : ?>
    <?php $image = $page->images->first ?>
    <div class="gallery">
        <img src="<?= $image->size(1000, 0)->url ?>" alt="<?= $image->description ?>">
    </div>
<?php endif ?>

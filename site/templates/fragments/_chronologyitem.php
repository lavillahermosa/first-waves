<?php 
$description = $item->text && strlen($item->text) > 0 ? true : false; 
?>

<li class="chronology__item<?= $description ? " chronology__item--description" : "" ?> js-chronologyItem" data-year="<?= $datetime->date('Y', $item->getUnformatted('chemins_date_begin')) ?>">
    <div class="chronology__item__inner">
        <div class="chronology__item__top">
            <div class="chronology__item__title">
                <?= $item->title ?>
            </div>
            <?php if($item->tags && $item->tags->count() > 0):?>
                <ul class="chronology__item__tags">
                    <?php foreach($item->tags as $tag):?>
                        <li class="chronology__item__tag"><?= $tag->title ?></li>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
            <!-- <div class="chronology__item__date">
                <?php $item->chemins_date_begin ?>
            </div> -->
        </div>

        <?php if($description): ?>
            <div class="chronology__item__bottom">
                <div class="chronology__item__description">
                    <?= $item->text ?>
                </div>
            </div>
        <?php endif ?>
        
    </div>
</li>
<?php
namespace ProcessWire;
?>

<div class="article__col__group">
    <h3 class="article__subtitle">
        <p><?= __("Document sound(s)", $tr) ?></p>
    </h3>
    <div class="article__videos">
        <?php foreach($page->videos as $video): ?> 
        <?php $thumbnail = $video->thumbnail ?>  
        <?php $video = $video->video_file ?>
        <?php $title = $video->description ? $video->description : $video->name; ?>              
        <div class="article__video">
            <div class="video js-player">
                <video title="<?= $title ?>" class="video__screen" poster="<?= $thumbnail->url ?>">
                    <source src="<?= $video->url ?>" type="video/mp4">
                </video>
                <div class="player__progress__container js-playerProgressContainer">
                    <div class="player__progress js-playerProgress"></div>
                </div>
                <div class="video__inner">
                    <div class="video__title">
                        <button class="video__play js-playerPlay">
                            <svg class="video__play__icon js-playIcon" viewBox="0 0 20 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 13V24L20 13L0 2V13Z" fill="black"/>
                            </svg>
                            <svg class="video__play__icon hidden js-pauseIcon" viewBox="0 0 16 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 2V24H6V2H0Z" fill="black"/>
                                <path d="M10 2V24H16V2H10Z" fill="black"/>
                            </svg>
                        </button>
                        <div class="video__title">
                            <p><?= $title ?></p>
                        </div>
                    </div>
                    <div class="video__times">
                        <div class="video__time js-playerCurrent">00:00</div>
                        <div class="video__time video__time--end js-playerTotal">00:00</div>
                    </div>
                </div>

            </div>                      
        </div>
        <?php endforeach ?>
    </div>
</div>
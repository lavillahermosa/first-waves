<?php
namespace ProcessWire;
require_once('_functions.php');

$pageCategory = getLinkedNodes($article, ["chemins-node-category"])->first;
$itemYear = $article->chemins_date_begin ? $datetime->date('Y', $article->getUnformatted('chemins_date_begin')) : "";

$pageyearsRange = yearsRange($article->chemins_date_begin, $article->chemins_date_end); 
$itemsSamePeriod = new PageArray();

if($pageyearsRange && !empty($pageyearsRange)){
    foreach($allItems as $item){
        $itemyearsRange = yearsRange($item->chemins_date_begin, $item->chemins_date_end);
        if(!empty($itemyearsRange) && array_intersect($itemyearsRange, $pageyearsRange) && $item->id !== $article->id) $itemsSamePeriod->add($item);
    }
}
?>

<?php foreach ($languages as $language): ?>
<?php endforeach ?>

<?php
/*
$french = $languages->get("fr");
$page->setOutputFormatting(false);
$pagetitle = $page->title->getLanguageValue($french);*/
?>

<article class="js-article<?= $isStacked ? "" : " open" ?>" 
data-href="<?= $article->url ?>" 
data-id="<?= $article->id ?>" 
>
    <div class="document__container js-documentContainer hidden">   
    </div>
    <div class="article__inner">
        <div class="article__title js-articleHeader">
            <h2 class="article__title__category">
                <?= $pageCategory->title ?>
            </h2>
            <div class="article__title__col">
                <h2><?= $article->title ?></h2>
                <div class="article__buttons">
                    <!-- <div class="article__button article__button--open js-articleOpen">↑</div>
                    <div class="article__button article__button--close js-articleOpen">↓</div> -->
                    <div class="article__button article__button--remove js-articleRemove">
                        <svg  viewBox="0 0 240 284" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0_170_261)">
                        <path d="M240 0H216.478L110 140.527V143.683L216.478 284H240L127.431 136.32L127.641 147.47L240 0Z" fill="black"/>
                        </g>
                        <g clip-path="url(#clip1_170_261)">
                        <path d="M-3.78489e-06 0H23.5218L130 140.527V143.683L23.5218 284H-3.78489e-06L112.569 136.32L112.359 147.47L-3.78489e-06 0Z" fill="black"/>
                        </g>
                        <defs>
                        <clipPath id="clip0_170_261">
                        <rect width="130" height="284" fill="black" transform="translate(110)"/>
                        </clipPath>
                        <clipPath id="clip1_170_261">
                        <rect width="130" height="284" fill="black" transform="matrix(-1 0 0 1 130 0)"/>
                        </clipPath>
                        </defs>
                        </svg>
                    </div>
                </div>                    
            </div>
        </div>
        <div class="article__main">
            <div class="article__col article__col--left">

                <?php 
                $documents = getLinkedNodes($article, ["chemins-node-document", "chemins-node-doc-story"], [], ['chemins-edge-link']);
                include('fragments/_documents.php'); ?>

                <?php
                $linkedNodes = getLinkedNodes($article, [], [], ['chemins-edge-relation']);
                $linkedNodes = $linkedNodes->find("template!=chemins-node-page");
                include('fragments/_links.php'); ?>

            </div>

            <div class="article__col article__col--right">
                <?php if($article->files && $article->files->count() > 0): ?>
                    <div class="article__col__group">
                        <h3 class="article__subtitle">
                            <p><?= $article->files->count() > 1 ? __("PDF files", $tr) : __("PDF file", $tr) ?></p>
                        </h3>
                        <?php 
                        $pdfs = $article->pdfs;
                        include('fragments/_PDF.php'); ?>
                    </div>
                <?php endif ?>

                <?php if($article->text && strlen($article->text) > 0): ?>
                    <div class="article__col__group article__description__container"> 
                        <?php if($article->chemins_date_begin || $article->chemins_date_end || $article->tags && $article->tags->count() > 0): ?>
                            <div class="article__description__filters">
                                <?php if($article->chemins_date_begin || $article->chemins_date_end) :?>
                                    <?php
                                    $yearBegin = $datetime->date('Y', $article->getUnformatted('chemins_date_begin'));
                                    $yearEnd = $datetime->date('Y', $article->getUnformatted('chemins_date_end')); ?>
                                    <div class="article__dates">
                                        <div class="article__date js-articleYear">
                                            <?php if($article->chemins_date_begin && $article->chemins_date_end && $yearBegin !== $yearEnd):?>
                                                <?= $yearBegin . "-" . $yearEnd ?>
                                            <?php elseif($article->chemins_date_begin): ?>
                                                <?= $yearBegin ?>
                                            <?php elseif($article->chemins_date_end): ?>
                                                <?= $yearEnd ?>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                <?php endif ?> 
                                <?php if($article->tags && $article->tags->count() > 0):?>
                                    <div class="article__tags">
                                        <?php $tags = array() ?>
                                        <?php foreach($article->tags as $tag):?>
                                            <div class="article__tag js-articleTag"
                                            data-category="<?= $tag->name ?>"
                                            >
                                                <?= $tag->title ?>
                                            </div>
                                        <?php endforeach ?>
                                    </div>
                                <?php endif ?>  
                            </div>
                        <?php endif ?>
                        
                        <div class="article__description js-description">     
                            <?= $article->text ?>
                            <?php $images = $article->images ?>
                            <?php if($images && $images->count > 0):?>
                                <div class="article__description__gallery<?= strlen($article->text) > 0 ? " article__description__gallery--marginTop" : "" ?>">
                                    <?php include('fragments/_gallery.php'); ?>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</article>
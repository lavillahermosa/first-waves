<?php
namespace ProcessWire;
require_once('_functions.php');
$documentParent = getLinkedNodes($page, [], [], ['chemins-edge-link']);
$documentCategory = getLinkedNodes($documentParent->first, ["chemins-node-category"])->first;
?>

<?php if(!$config->ajax): ?>
<main id="content" class="content js-content">
<?php endif; ?>

    <div class="article__document <?=$page->template->name?><?= strlen($page->text) < 400 ? " open": "" ?>">
        <div class="article__title">
          
            
            <div class="article__title__inner--document">
                <h2 class="article__title__category">
                    <?= __("document ", $tr) ?>
                </h2>
            <div class="article__title__col">
                    <h2><?= $page->title ?></h2>
                    <div class="article__buttons">
                        <!-- <div class="article__button article__button--open js-articleOpen">↑</div>
                        <div class="article__button article__button--close js-articleOpen">↓</div> -->
                        <div class="article__button article__button--remove js-documentClose">
                            <svg  viewBox="0 0 240 284" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_170_261)">
                            <path d="M240 0H216.478L110 140.527V143.683L216.478 284H240L127.431 136.32L127.641 147.47L240 0Z" fill="black"/>
                            </g>
                            <g clip-path="url(#clip1_170_261)">
                            <path d="M-3.78489e-06 0H23.5218L130 140.527V143.683L23.5218 284H-3.78489e-06L112.569 136.32L112.359 147.47L-3.78489e-06 0Z" fill="black"/>
                            </g>
                            <defs>
                            <clipPath id="clip0_170_261">
                            <rect width="130" height="284" fill="black" transform="translate(110)"/>
                            </clipPath>
                            <clipPath id="clip1_170_261">
                            <rect width="130" height="284" fill="black" transform="matrix(-1 0 0 1 130 0)"/>
                            </clipPath>
                            </defs>
                            </svg>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="article__main">
            <!-- check if col is empty  -->
            <?php if($page->pdfs && $page->pdfs->count() > 0 || $page->videos && $page->videos->count > 0 || $page->sound_files && $page->sound_files->count > 0  || $page->images && $page->images->count > 0): ?>
                <div class="article__col article__col--right">
                    
                    <?php if($page->pdfs && $page->pdfs->count() > 0): ?>
                        <div class="article__col__group">
                            <h3 class="article__subtitle">
                                <p><?= __("PDF file", $tr) ?></p>
                            </h3>
                            <div class="article__pdf">
                                <?php 
                                $pdfs = $page->pdfs;
                                include('fragments/_PDF.php'); ?>
                            </div>
                        </div>
                    <?php endif ?>
                    <?php if($page->videos && $page->videos->count > 0): ?>
                        <?php include('fragments/_videos.php') ?>
                    <?php endif ?>
                    <?php if($page->sound_files && $page->sound_files->count > 0): ?>
                        <?php include('fragments/_sounds.php') ?>
                    <?php endif ?>
                    <?php if($page->images && $page->images->count > 0):?>
                        <div class="article__col__group">                
                            <h3 class="article__subtitle">
                                <p><?= __("Document images", $tr) ?></p>
                            </h3>
                            <?php $images = $page->images ?>
                            <?php include('fragments/_gallery.php'); ?>
                        </div>
                    <?php endif ?>
                </div>
            <?php endif ?>
            <!-- check if col is empty  -->
            <?php
            $jsonInput = json_decode(file_get_contents("php://input"), true);
            $sourceArticleId = ($jsonInput)?$jsonInput['sourceArticleId']:null;
            $linkedNodes = getLinkedNodes($page, [], [], ['chemins-edge-relation', 'chemins-edge-link']);
            $linkedNodes = $sourceArticleId ? $linkedNodes->find("template!=chemins-node-page, id!=$sourceArticleId") : $linkedNodes->find("template!=chemins-node-page");
            ?>
            
            <?php if($page->text && strlen($page->text) > 0 || $page->videos && $page->videos->count > 0 || $page->sound_files && $page->sound_files->count > 0  || $linkedNodes->count > 0 && $linkedNodes->first->id !== 0): ?>
                <div class="article__col article__col--left">
                    <?php if($page->text && strlen($page->text) > 0): ?>
                        <div class="article__col__group">                
                            <h3 class="article__subtitle">
                                <p><?= __("Article description", $tr) ?></p>
                            </h3>
                            <?php if($page->tags && $page->tags->count() > 0):?>
                                    <div class="article__tags">
                                        <?php $tags = array() ?>
                                        <?php foreach($page->tags as $tag):?>
                                            <?php $tags[] = $tag->title ?>
                                        <?php endforeach ?>
                                        <?= "tags: " . implode(", ", $tags) ?>
                                    </div>
                                <?php endif ?> 
                            <div class="article__description js-description">    
                                <?= $page->text ?>
                            </div>
                        </div>
                    <?php endif ?>

                    <?php include('fragments/_links.php'); ?>

                </div>
            <?php endif ?>
        </div>
    </div>

<?php if(!$config->ajax):?>
</main>
<?php else:return $this->halt(); endif; ?>



<?php
namespace ProcessWire;
require_once('_functions.php');
$parent = $pages->get('/nodes');
$allItems = $parent->find("parent=$parent->children")->find("template!=chemins-node-category, template!=chemins-nodes-category, template!=chemins-node-page, template!=chemins-node-document");
$articlesID = isset($_GET['items']) ? $_GET['items'] : "";

function yearsRange($yearBegin, $yearEnd){
    $itemyearsRange = array();
    if($yearBegin && $yearEnd){
        $itemyearsRange = range(date('Y', strtotime($yearBegin)), date('Y', strtotime($yearEnd)));
    }else if($yearBegin){
        $itemyearsRange[] = date('Y', strtotime($yearBegin));
    }
    return $itemyearsRange;
}

?>

<?php if(!$config->ajax): ?>
<main id="content" class="js-content<?= isset($_GET["articlesClosed"]) ? " articlesClosed" : "" ?>">
<?php endif; ?>

    <?php if(isset($articlesID) && $articlesID): ?>
        <?php $IDs = explode(",", $_GET["items"]) ?>
        <?php foreach($IDs as $articleID):?>
            <?php 
            $allItems = $parent->find("parent=$parent->children")->find("template!=chemins-node-category, template!=chemins-nodes-category, template!=chemins-node-page, template!=chemins-node-document"); 
            $article = $pages->get($articleID);
            $isStacked = $articleID == $page->id && !isset($_GET["articlesClosed"]) ? false : true;
            ?>
            <?php include("_articlecontent.php") ?>
        <?php endforeach ?>
    <?php else: ?>
        <?php 
            $url = (empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            $url_parts = parse_url($url);
            isset($url_parts['query']) ? parse_str($url_parts['query'], $params) : $params = array();
            $params = array();
            $params['items'] = $page->id;
            $url_parts['query'] = http_build_query($params);
            $newURL = $url_parts['scheme'] . '://' . $url_parts['host'] . $url_parts['path'] . '?' . $url_parts['query'];
            header("Location: " . $newURL);
            exit;
            $article = $page;
            $isStacked = false;
        ?>
        <?php include("_articlecontent.php") ?>
    <?php endif ?>

<?php if(!$config->ajax):?>
</main>
<?php else:return $this->halt(); endif; ?>

<?php
namespace ProcessWire;
$categories = wire('pages')->get('/nodes/categories/')->children;
$tags = wire('pages')->get('/tags/')->children;
require_once('_functions.php');
$pageCategory = getLinkedNodes($page, ["chemins-node-category"]);
$parent = $pages->get('/nodes');
$allItems = $parent->find("parent=$parent->children")->find("template!=chemins-node-category, template!=chemins-nodes-category, template!=chemins-node-page, template!=chemins-node-document");
$firstYear = $allItems->filter("!chemins_date_begin=")->sort("chemins_date_begin")->first->chemins_date_begin;
$lastYear = $allItems->filter("!chemins_date_end=")->sort("chemins_date_end")->last->chemins_date_end;
$allYears = range(date('Y', strtotime($firstYear)), date('Y', strtotime($lastYear)));
$allYears = array_unique($allYears);
$indexItems = $parent->find("parent=$parent->children")->find("template!=chemins-node-category, template!=chemins-nodes-category, template!=chemins-node-page, template!=chemins-node-document, template!=chemins-node-doc-story")->sort("title");
?>

<nav id="main-nav" class="js-nav init">
    <div class="nav__top">
        <div class="nav__col">
            <div class="nav__button">
                <div class="nav__button__close js-navButton">
                    <div class="hamburger js-hamburger">
                        <svg width="23" height="12" viewBox="0 0 23 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 1H23" stroke="black" stroke-width="1"/>
                            <path d="M0 5H23" stroke="black" stroke-width="1"/>
                            <path d="M0 9H23" stroke="black" stroke-width="1"/>
                        </svg>
                    </div>
                </div>
                <div class="nav__button__open">
                    <div class="hamburger--close js-hamburger js-navButton">
                        <svg width="25" height="12" viewBox="0 0 25 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 1L24 9" stroke="black" stroke-width="0.9"/>
                            <path d="M1 9L24 1" stroke="black" stroke-width="0.9"/>
                        </svg>
                    </div>
                </div>
            </div>
            <?php $articlesID = isset($_GET['items']) ? $_GET['items'] : "";?>
            <ul class="nav__languages">
                <?php foreach ($languages as $language): ?>
                    <?php 
                    $languageURL = $page->localUrl($language);
                    $URL = "";
                    if(isset($_GET["articlesClosed"]) && isset($articlesID) && $articlesID){
                        $URL = $page->localUrl($language) . "?items=" . $articlesID . "&articlesClosed=true";
                    }elseif(isset($articlesID) && $articlesID){
                        $URL = $page->localUrl($language) . "?items=" . $articlesID;
                    }else{
                        $URL = $languageURL;
                    }
                    ?>
                
                    <li class="nav__language">
                        <a
                        class="nav__language__link js-navLang<?= $language->id == $user->language->id ? " active" : "" ?>" 
                        href="<?= $URL ?>"
                        data-id="<?= $language->id ?>"
                        >
                            <?= $language->title ?>
                        </a>
                    </li>
                <?php endforeach ?>
                <li class="nav__language">
                    <a class="nav__language__link disabled" href="#">NL</a>
                </li>
            </ul>       
        </div>
        <div class="nav__col">
            <a href="<?= $home->url ?>"><?= "FIRST WAVES" ?></a>
        </div>
        <div class="nav__col">
            <a class="js-aboutBtn" href="<?= wire('pages')->get('/about/')->url ?>"><?= __("About", $tr) ?></a>
        </div>
    </div>
    
    <div class="menus">
        <div class="menus__inner">
            <div class="menu index__container js-navList js-navBody active">
                <div class="index__filters__container">
                    <ul class="index__filters index__filters--category js-filterCategories">
                        <div class="index__filters__title">
                            <?= __("Categories", $tr) ?>
                        </div>
                        <?php foreach($categories as $category): ?>
                            
                            <?php if(getLinkedNodes($category)->find("template!=chemins-node-page, template!=chemins-node-document")->count() > 0): ?>

                                <?php $pageCategory = getLinkedNodes($page, ["chemins-node-category"])->first; ?>
                                <?php $linkedNodes = getLinkedNodes($category); ?>
                                <li class="index__filter js-filter" data-category="<?= $category->name ?>">
                                    <span> <?=  $category->title ?> </span>
                                </li>

                            <?php endif ?>

                        <?php endforeach ?>
                    </ul>
                    <ul class="index__filters index__filters--tags js-filterCategories">
                        <div class="index__filters__title">
                            <?= __("Tags", $tr) ?>
                        </div>
                        <?php foreach($tags as $tag): ?>
                            <?php if($indexItems->find("tags=$tag")->count()): ?>
                                <li class="index__filter js-filter" data-category="<?= $tag->name ?>">
                                    <span> <?= $tag->title ?> </span>
                                </li>
                            <?php endif ?>
                        <?php endforeach ?>
                    </ul>
                    <ul class="index__filters index__filters--years js-filterYears">
                        <div class="index__filters__title">
                            <?= __("Years", $tr) ?>
                            </div>
                        <?php foreach($allYears as $year): ?>
                            <li class="index__filter index__filter--year js-filter js-filterYear" data-year="<?= $year ?>">
                                <span> <?= $year ?> </span>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
                
                <div class="index">
                    <?php foreach($indexItems as $item): ?>
                        <?php 
                            $itemCategories = getLinkedNodes($item, ["chemins-node-category"]); 
                            $itemCategoriesNames = [];
                            foreach ($itemCategories as $itemCategory){
                                $itemCategoriesNames[] = $itemCategory->name;
                            };
                            $tags = $item->tags;
                            if($tags->count() > 0){
                                $arrTags = [];
                                foreach($tags as $tag){
                                    $arrTags[] = $tag->name;
                                };
                            };
                        ?>
                        <li class="index__item js-indexItem <?= implode(" ", $itemCategoriesNames) ?> <?= $tags->count() > 0 ? implode(" ", $arrTags) : "" ?>" 
                        data-year="<?= $item->chemins_date_begin ? $datetime->date('Y', $item->getUnformatted('chemins_date_begin')) : "" ?>" 
                        data-yearbegin="<?= $item->chemins_date_begin ? $datetime->date('Y', $item->getUnformatted('chemins_date_begin')) : "" ?>" 
                        data-yearend="<?= $item->chemins_date_end ? $datetime->date('Y', $item->getUnformatted('chemins_date_end')) : "" ?>"
                        data-id="<?= $item->id ?>" 
                        >
                            <a href="<?= $item->url() ?>" class="index__item__link">
                                <?= $item->title ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>  

    <div class="about js-about init">
        <div class="about__inner">
        <div class="about__list">
                <?= $pages->get('/about/')->credits ?>
            </div>
            <div class="about__description js-about">
                <?= $pages->get('/about/')->text ?>
            </div>


        </div>
        
    </div>
</nav>

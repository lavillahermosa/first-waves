<?php
namespace ProcessWire;

function formatBytes($bytes, $precision = 2) { 
    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

    $bytes = max($bytes, 0); 
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
    $pow = min($pow, count($units) - 1); 

    $bytes /= pow(1024, $pow);

    return round($bytes, $precision) . $units[$pow]; 
} 
?>

<?php foreach ($pdfs as $pdf): ?>
    <?php 
    $image = $pdf->toImage();
    ?>
    <div class="article__pdf__viewer">
        <iframe
        src="<?= $pdf->url ?>"
        loading="lazy"
        width="100%"
        title="<?= $pdf->description ? $pdf->description : $page->title ?>"
        ></iframe>
    </div>
    <?php 
    $title;
    if($pdf->description){
        $title = __("Download the PDF", $tr) . (" ") . $pdf->description . " (" . formatBytes($pdf->filesize) . ")";
    }else{
        $title =  __("Download the PDF", $tr) . (" ") . $pdf->basename() . " (" . formatBytes($pdf->filesize) . ")";
    }
    ?>
    <a class="article__pdf__button" href="<?= $pdf->url() ?>" download><?= $title ?></a>
<?php endforeach ?>
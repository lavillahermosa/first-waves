<?php namespace Processwire;

if($images->count > 1) : ?>
<ul class="gallery">
    <?php foreach($images as $image):?>
        <li class="gallery__item<?= $image->ratio() < 1 ? " gallery__item--portrait" : "" ?> gallery__item--zoomable js-imageZoom">
            <div class="gallery__item__inner">
                <img class="lazyload" data-srcbig="<?= $image->size(1920, 0)->url ?>" data-src="<?= $image->size(400, 0)->url ?>" alt="<?= $image->description ?>">
            </div>
        </li>
    <?php endforeach?>
</ul>
<?php else : ?>
    <?php $image = $images->first ?>
    <div class="gallery gallery--oneImage document--zoomable">
        <div class="gallery__item<?= $image->ratio() < 1 ? " gallery__item--portrait" : "" ?> gallery--zoomable js-imageZoom">
            <div class="gallery__item__inner">
                <img class="lazyload" data-src="<?= $image->size(1000, 0)->url ?>" data-srcbig="<?= $image->size(1920, 0)->url ?>" alt="<?= $image->description ?>">
            </div>
        </div>
    </div>
<?php endif ?>

<?php namespace ProcessWire;

/**
 * TRANSLATABLE STRINGS
 * Define globally available translatable strings
 *
 * USAGE
 * place this file under /site/translations.php
 * The wire property $ferry refers to the textdomain of this file
 *
 */

//__("all editions", $tr);


__('Next');
__('Previous');
__('About First Waves');
__('About');
__('Categories');
__('Index');
__('Tags');
__('Years');
__('Close');
__('PDF file');
__('PDF files');
__("Article description");
__("Document images");
__("At the same period");
__("Linked elements");
__("Document video");
__("Document sound(s)");
__("Download the PDF");
__("Images");
__("Video");
__("Sound");
__("Text");

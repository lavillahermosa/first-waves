<?php
namespace ProcessWire;

if($config->ajax){
    $prefix = $this->modules->getConfig('Chemins', 'prefix');
    $content = trim(file_get_contents("php://input"));
    $request = json_decode($content, true);
    $output = [];

    if(!isset($request['action'])){
        exit();
    }
    switch($request['action']){
        case 'getNodes':
            $output = getNodes($prefix);
            break;
        case 'getNode':
            $output = getNode($prefix, $request['id']);
            break;
        case 'getNodeTypes':
            $output = getNodeTypes($prefix);
            break;
        case 'getEdgeTypes':
            $output = getEdgeTypes($prefix);
            break;
        case 'saveNode':
            $output = saveNode($prefix, $request['title'], $request['parentId'], $request['template'], $request['x'], $request['y']);
            break;
        case 'removeNode':
            $output = deletePage($request['id']);
            break;
        case 'getEdges':
            $output = getEdges($prefix);
            break;
        case 'getEdge':
            $output = getEdge($prefix, $request['id']);
            break;
        case 'saveEdge':
            $output = saveEdge($prefix, $request['title'], $request['parentId'], $request['template'], $request['nodeAId'], $request['nodeBId']);
            break;
        case 'removeEdge':
            $output = deletePage($request['id']);
            break;
        case 'upEdge':
            $output = pageSortLast($request['id']);
            break;
        case 'updatePage':
            $output = updatePage($prefix, $request['id'], $request['data']);
            break;
        case 'getControl':
            $output = getControlStatus($prefix);
            break;
    }

    echo json_encode($output);

}else{
?>


<span class="loader-font loader-font-1">load</span>
<section class="chemins-container">
    <section class="tools-ui">
        <ul class="tools-buttons">
            <li class="button tool-plus"  data-tool="add-node"></li>
            <li class="button tool-line" data-tool="add-edge"></li>
            <li class="button tool-move" data-tool="move-node"></li>
            <li class="button tool-rename" data-tool="rename"></li>
            <li class="button tool-depth" data-tool="up-depth"></li>
            <li class="button tool-invert" data-tool="reverse-edge"></li>
            <li class="button tool-open" data-tool="open-form"></li>
            <li class="button tool-delete" data-tool="delete"></li>
        </ul>
        <ul class="tools-descriptions">
            <li class="description" data-tool="add-node">
                Add nodes to the map
            </li>
            <li class="description" data-tool="add-edge">
                Add edges between nodes
            </li>
            <li class="description" data-tool="move-node">
                Move nodes
            </li>
            <li class="description" data-tool="rename">
                Rename node or edge
            </li>
            <li class="description" data-tool="up-depth">
                Bring object to top
            </li>
            <li class="description" data-tool="reverse-edge">
                Change direction of edge
            </li>
            <li class="description" data-tool="open-form">
                Open form in popup
            </li>
            <li class="description" data-tool="delete">
                Delete node or edge
            </li>
        </ul>
    </section>
    <div class="view-ui">
    </div>
    <section class="map-ui">
        <div class="space-arrow arrow-top" data-direction="top"></div>
        <div class="space-arrow arrow-right" data-direction="right"></div>
        <div class="space-arrow arrow-bottom" data-direction="bottom"></div>
        <div class="space-arrow arrow-left" data-direction="left"></div>

    </section>
    <section class="info-display"></section>
</section>
<script type="module" src="<?=$this->config->urls->Chemins?>init.js"></script>
<?php
}
?>
import { drawFromPoints, computeLine, showPoints } from "./Draw.js";


const Edge = ($container, {
    id = null, 
    title = null,
    edgeType = null,
    nodeA = null,
    nodeB = null,
    disabled = false

    } = {}) => {

        const snapElem = Snap();
        const $edge = document.createElement('div');
        const $title = document.createElement('h3');

        

        $edge.classList.add('edge');
        const strokeWidth = 2, definition = 12, canvasSize = 50, strokeOverflow = 5,  strokeColor=edgeType?edgeType.color:'#000000';
        let width, height, x, y, angle, z;
        let shapePoints, shape;

        const init = () => {
            $edge.appendChild($title);
            $title.innerHTML = `<span>${title}</span>`;
            $title.classList.add('action');
            $title.setAttribute('data-action', 'renameEdge');
            $title.setAttribute('data-action-type', 'dblclick');
            $edge.setAttribute('data-id', id);
            $edge.appendChild(snapElem.node);
            $container.appendChild($edge);

            update();
            updateVisibility();
            
        };

        const update = () => {
            let pos = computePosition();
            setPosition(pos);
            snapElem.attr({
                width:width,
                height:height
            });
            
           
            shapePoints = computeShapePoints();
            if(shape != null){
                shape.remove();

            }
            shape = drawFromPoints(snapElem, shapePoints, edgeType.border, false);
            shape.attr({
                display:'inline',
                stroke:strokeColor,
                fill:'transparent',
                strokeWidth:strokeWidth
            });
        };
        const reverse = () => {
            const nodeTmp = nodeA;
            nodeA = nodeB;
            nodeB = nodeTmp;
            update();
        };

        const setPosition = ({
            newX = x, newY = y, 
            newWidth = width, newHeight = height, 
            newAngle = angle
        } = {}) => {
            x = newX;
            y = newY;
            width = newWidth;
            height = newHeight;
            angle = newAngle;

            $edge.style.left = `${x}px`;
            $edge.style.top = `${y}px`;
            $edge.style.width = `${width}px`;
            $edge.style.height = `${height}px`;
            $edge.style.transform = `rotate(${(angle/Math.PI)*180}deg)`;
            //console.log("angle", (Math.abs(Math.cos(angle)) * Math.abs(Math.sin(angle)))*2);
            //console.log($edge);

        };

        const updateVisibility = () => {
            (edgeType.visible)?$edge.classList.remove('hidden'):$edge.classList.add('hidden');
        };
        
        const removeElement = () => {
            $edge.remove();
        };

        const disable = () => {
            disabled = true;
            $edge.classList.add('disabled');
        }
        const enable = () => {
            disabled = false;
            $edge.classList.remove('disabled');
        }
        const computeShapePoints = () => {
            const startPoint = nodeA.getBorderPoint(angle, strokeOverflow);
            const endPoint = nodeB.getBorderPoint(angle-Math.PI, strokeOverflow);
           
            if(startPoint == false || endPoint == false)
                return false;
                
            const startDist = Math.sqrt((Math.pow(startPoint.x-nodeA.width/2,2))+(Math.pow(startPoint.y-nodeA.height/2,2))) + nodeA.margin;
            const endDist =  Math.sqrt((Math.pow(endPoint.x-nodeB.width/2,2))+(Math.pow(endPoint.y-nodeB.height/2,2))) + nodeB.margin;
            //min2+(max2-min2)*((variable1-min1)/(max1-min1))
            
            //a value between 0 1
            const marginAngle = Math.abs(Math.cos(angle)) * Math.abs(Math.sin(angle))*2;
            

            
            $title.style.paddingLeft = startDist+marginAngle*height+"px";
            $title.style.paddingRight = endDist+marginAngle*height+"px";
            //la ligne est rectiligne par défaut

            return computeLine(startDist, height/2, width-endDist, height/2, definition);
            
        };

        const computePosition = () => {
            //va falloir définir les deux points du segment
            //en absolu en fonction des tracés des nodes.

            //on part de la position en xy du milieu de chacun des nodes
            const middlePosNodeA = {
                'x':nodeA.x + nodeA.width/2,
                'y':nodeA.y + nodeA.height/2
            };
            const middlePosNodeB = {
                'x':nodeB.x + nodeB.width/2,
                'y':nodeB.y + nodeB.height/2
            };


            const angle = Math.atan2(middlePosNodeB.y-middlePosNodeA.y, middlePosNodeB.x-middlePosNodeA.x);
            const distance = Math.sqrt((Math.pow(middlePosNodeB.x-middlePosNodeA.x,2))+(Math.pow(middlePosNodeB.y-middlePosNodeA.y,2)));
            return {
                newX:middlePosNodeA.x, 
                newY:middlePosNodeA.y - canvasSize/2,
                newWidth:distance,
                newHeight:canvasSize,
                newAngle:angle
            };



        };

        init();

        return {
            set id(value) {
                id = value;
                $edge.setAttribute('data-id', id);
            },
            set title(value){
                title = value;
                $title.innerHTML = `<span>${title}</span>`;
            },
            set z(value){
                z = value;
                $edge.style.zIndex = z;
            },
            set nodeA(value){
                nodeA = value;
                update();
            },
            set nodeB(value){
                nodeB = value;
                update();
            },
            
            get id (){return id},
            get $edge(){return $edge}, 
            get nodeA() {return nodeA},
            get nodeB() {return nodeB},
            get edgeType(){return edgeType},
            get title(){return title},
            get disabled(){return disabled},
            get x(){return x},
            get y(){return y},
            get z(){return z},
            enable, disable, updateVisibility,
            update, reverse, removeElement};

};

export default Edge;
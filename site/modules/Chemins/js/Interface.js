import Node from "./Node.js";
import Edge from "./Edge.js";
import router from "./Router.js";
import session from "./Session.js";
import eventsHandler from "./EventsHandler.js";

const Interface = (() => {

    let $scene, $spaceArrows, $infoDisplay, $viewButton;

    let tools;

    let dataNodeTypes, dataEdgeTypes;

    let nodes = [], edges = [];
    
    const nodesById = {}, edgesById = {}, edgesByNodeId = {};
    
    let selectedNodes = [];
    
    const graphSize = {'width':0, 'height':0};

    //how much empty space on the bottom and the left 
    const graphMargin = 100;


    const init = async() => {
        $scene = document.querySelector('section.chemins-container');
        $infoDisplay = document.querySelector('.info-display');
        $spaceArrows = document.querySelectorAll('.space-arrow');
        $viewButton = document.querySelector('.view-ui');

        dataNodeTypes = await router.getNodeTypes?.();
        dataEdgeTypes = await router.getEdgeTypes?.();
        
        const dataNodes = await router.getNodes?.();
        buildNodes(dataNodes);
        const dataEdges = await router.getEdges?.();
        buildEdges(dataEdges);

        tools = Tools();

        eventsHandler.registerItfEvents();
        
        
        setSceneSize();
  
    }


    const addSpace = (direction) => {
        if(!session.hasControl()) return;
        switch(direction){
            case 'top':
                nodes.forEach(node => {
                    node.y += 100;
                    updateLinkedEdges(node);
                    router.updatePage(node.id, {'x':node.x, 'y':node.y}); 
                });
                setSceneSize();
                break;
            case 'left':
                nodes.forEach(node => {
                    node.x += 100;
                    updateLinkedEdges(node);
                    router.updatePage(node.id, {'x':node.x, 'y':node.y}); 
                });
                setSceneSize();
                break;
            case 'right':
                $scene.style.width = $scene.offsetWidth + 100 + 'px';
                window.scrollBy(100, 0);
                break;
            case 'bottom':
                $scene.style.height = $scene.offsetHeight + 100 + 'px';
                window.scrollBy(0, 100);
                break;
            
        }
    };


    const toggleSpaceArrows = ({top = false, right = false, bottom = false, left = false} = {}) => {
        $spaceArrows.forEach($spaceArrow => {
            const direction = $spaceArrow.getAttribute('data-direction');
            if(eval(direction)){
                $spaceArrow.classList.add('active');
            }else{
                $spaceArrow.classList.remove('active');
            }
        });
    }

    //######################TOOLS
    const Tools = ({defaultTool = 'move-node'} = {}) => {


        let $toolsUI, $toolsButtons, $toolsDesc;
        let currentTool = defaultTool;


        const init = () => {
            $toolsUI = document.querySelector('.tools-ui');
            $toolsButtons = $toolsUI.querySelectorAll('.button');
            $toolsDesc = $toolsUI.querySelectorAll('.description');
            switchTool(currentTool);
            
        };

        const showUI = () => {
            $toolsUI.classList.add('show');
        };
        const hideUI = () => {
            $toolsUI.classList.remove('show');
        };
        const showDesc = (tool) => {
            $toolsDesc.forEach($d => $d.getAttribute('data-tool') == tool?$d.classList.add('show'):$d.classList.remove('show'));
        };
        const hideDesc = () => {
            $toolsDesc.forEach($d => $d.classList.remove('show'));
        }

        const switchTool = (toolName) => {
            $toolsButtons.forEach(
                $b => $b.getAttribute('data-tool') == toolName?
                    $b.classList.add('active'):$b.classList.remove('active')
            );
            
            $scene.classList.remove(`tool-${currentTool}`);
            $scene.classList.add(`tool-${toolName}`);
            currentTool = toolName;
        };

       

        init();

        return {
            switchTool,
            showUI, hideUI, showDesc, hideDesc,
            get $toolsUI(){
                return $toolsUI;
            },
            get $toolsButtons(){
                return $toolsButtons;
            },
            get current(){
                return currentTool;
            }
        };

    };
    //############################END TOOLS



    const updateView = (viewData) => {

        Object.keys(viewData).forEach(id => {
            dataEdgeTypes[id].visible = viewData[id];
        });
        edges.forEach(edge => {
            edge.updateVisibility();
        });
    };


    const setSceneSize = () => {
        const sceneWidth = (window.screen.width > graphSize.width + graphMargin)?window.screen.width:graphSize.width + graphMargin;
        const sceneHeight = (window.screen.height > graphSize.height + graphMargin)?window.screen.height:graphSize.height + graphMargin;
        $scene.style.width = sceneWidth+'px';
        $scene.style.height = sceneHeight+'px';
    }

    const createEdge = (parentId, nodeA, nodeB, title, id = null) => {
        const edgeType = dataEdgeTypes[parentId];
        const edge = Edge($scene, {id:id, title:title, nodeA:nodeA, nodeB:nodeB, edgeType:edgeType});
        eventsHandler.registerEdgeEvents(edge);
        return edge;
    };


    const registerEdge = (edge) => {
        edges.push(edge);
        edge.z = edges.length;
        edgesById[edge.id] = edge;
        registerEdgeByNode(edge, edge.nodeA);
        registerEdgeByNode(edge, edge.nodeB);
    };

    const registerEdgeByNode = (edge, node) => {
        (edgesByNodeId[node.id] == undefined)?
            edgesByNodeId[node.id] = [edge]:
            edgesByNodeId[node.id].push(edge);
        
    };


    const removeEdge = async(edge) => {

        edge.removeElement();
    
        delete edgesById[edge.id];
        
        edgesByNodeId[edge.nodeA.id] = edgesByNodeId[edge.nodeA.id].filter(nodeEdge => nodeEdge != edge);
        edgesByNodeId[edge.nodeB.id] = edgesByNodeId[edge.nodeB.id].filter(nodeEdge => nodeEdge != edge);
        
        edges = edges.filter(item => item != edge);

        for(let i = edge.z; i < edges.length; i++){
            edges[i].z = i;
        }

    };

    const upEdge = async(edge) => {
        let previousZ = edge.z;
        for(let i = previousZ; i < edges.length; i++){
            edges[i].z = i;
        }
        edges.push(edges.splice(previousZ-1, 1)[0]);
        edge.z = edges.length;

    };

    const changeLinkedNodes = (edge, nodeAId, nodeBId) => {
        if(edge.nodeA.id != nodeAId){
            edgesByNodeId[edge.nodeA.id] = edgesByNodeId[edge.nodeA.id].filter(nodeEdge => nodeEdge != edge);
            registerEdgeByNode(edge, nodesById[nodeAId]);
            edge.nodeA = nodesById[nodeAId];
        }
        if(edge.nodeB.id != nodeBId){
            edgesByNodeId[edge.nodeB.id] = edgesByNodeId[edge.nodeB.id].filter(nodeEdge => nodeEdge != edge);
            registerEdgeByNode(edge, nodesById[nodeBId]);
            edge.nodeB = nodesById[nodeBId];
        }
    };

    const updateLinkedEdges = (node) => {
        
        const linkedEdges = edgesByNodeId[node.id];
        
        if(linkedEdges == undefined)
            return;
        linkedEdges.forEach(linkedEdge => {
            linkedEdge.update();
        });
    };

    const disableLinkedEdges = (node) => {
        const nodeEdges = edgesByNodeId[node.id];
        if(!nodeEdges)return;
        nodeEdges.forEach(edge => {
            edge.disable();

            if(edge.nodeA == node){
                //disable nodeB
                edge.nodeB.disable();
            }
            else if(edge.nodeB == node){
                //disable nodeA
                edge.nodeA.disable();
            }
        });
    };

    const enableLinkedEdges = (node) => {
        const nodeEdges = edgesByNodeId[node.id];
        if(!nodeEdges)return;
        nodeEdges.forEach(edge => {
            edge.enable();

            if(edge.nodeA == node){

                edge.nodeB.enable();
            }
            else if(edge.nodeB == node){

                edge.nodeA.enable();
            }
        });
    };

    const createNode = (title, parentId, x, y, id = null) => {
        const nodeType = dataNodeTypes[parentId];
        const node = Node($scene, 
            {id:id, title:title, x:x, y:y, nodeType:nodeType}
        );

        if(node.x + node.width > graphSize.width){
            graphSize.width = node.x + node.width;
        }
        if(node.y + node.height > graphSize.height){
            graphSize.height = node.y + node.height;
        }
        eventsHandler.registerNodeEvents(node);
        return node;
    };

    const registerNode = (node) => {
        nodes.push(node);
        nodesById[node.id] = node;
    };

    const selectNode = (node) => {
        if(selectedNodes.includes(node)){
            return false;
        }
        selectedNodes.push(node);
        node.select();
        return true;
    };
    const unselectNode = (node) => {
        selectedNodes = selectedNodes.filter(sNode => !sNode == node);
        node.unselect();
    };

    const unselectNodes = () => {
        const unselectedNodes = [];
        selectedNodes.forEach(node => {
            if(node.selected){
                unselectedNodes.push(node);
            }
            node.unselect();
        });
        selectedNodes = [];
        return unselectedNodes;
    };

    const removeNode = (node) => {
        node.removeElement();
        nodes = nodes.filter(item => node != item);
        delete nodesById[node.id];


    };

    const buildNodes = (dataNodes) => {
        const graphSize = {width:0, height:0};
        dataNodes.forEach(dataNode => {
            const node = createNode(dataNode.title, dataNode.parent, dataNode.x, dataNode.y, dataNode.id);
            registerNode(node);
        });
        return graphSize;
    };

    const buildEdges = (dataEdges) => {
        dataEdges.forEach(dataEdge => {
            if(nodesById[dataEdge.nodeA] != null && nodesById[dataEdge.nodeB] != null){
                const edge = createEdge(dataEdge.parent, 
                    nodesById[dataEdge.nodeA], nodesById[dataEdge.nodeB], dataEdge.title, dataEdge.id);
                registerEdge(edge);
            }
        });
    };

   
    const edgeForm = async({id=null, title=null, type=null} = {}) => {
        const edgeTypesOptions = Object.values(dataEdgeTypes).map(dataEdgeType => {
            return {'value':dataEdgeType.parent, 'name':dataEdgeType.name}
        });
        const formTitle = id ? 'edit edge': 'new edge';
        return await modalForm({id:'edge-form', title:formTitle, focus:'title', fields:[
            {type:'textarea', 'label':'edge title', 'name':'title', 'default':title},
            {type:'select', 'label': 'edge type', 'name':'type', 'default':type, 'options':edgeTypesOptions}
        ]}).then(output => {
            return output;
        }).catch(reason => {
            return null;
        });
    };

    const nodeForm = async({id=null, title=null, type=null} = {}) => {
        
        const nodeTypesOptions = Object.values(dataNodeTypes).map(dataNodeType => {
            return {'value':dataNodeType.parent, 'name':dataNodeType.name}
        });
        const formTitle = id ? 'edit node': 'new node';
        return await modalForm({id:'node-form', title:formTitle, focus:'title', fields:[
            {type:'textarea', 'label':'node title', 'name':'title', 'default':title},
            {type:'select', 'label': 'node type', 'name':'type', 'default':type, 'options':nodeTypesOptions}
        ]}).then(output => {
            return output;
        }).catch(reason => {
            return null;
        });
    };
    
    const viewForm = async() => {
        
        const edgeTypesOptions = Object.values(dataEdgeTypes).map(dataEdgeType => {
            
            return {'type':'checkbox', 'label': dataEdgeType.name, 'checked':dataEdgeType.visible, 'name':dataEdgeType.parent}
        });

        return await modalForm({id:'view-form', title:'select visible edge types', fields: edgeTypesOptions});


    };

    //creates modalform on the fly, returns a promise with the filled fields on validation
    const modalForm = ({id = 'modal-form', title = null, focus = null, fields = []} = {}) => {
        return new Promise((resolve, reject) => {
            const $modal = document.createElement('div');
            $modal.classList.add('modal');

            let form = '';
            fields.forEach(field => {
                form += '<div>';
                if(field.label){
                    form += `<label>${field.label}</label>`;
                }
                switch(field.type){
                    case 'textarea':
                        form += `<textarea name="${field.name}">${field.default?field.default:''}</textarea>`;
                        break;
                    case 'select':
                        form += `<select name="${field.name}">`;
                        field.options.forEach(option => {
                            form += `<option ${(field.default && field.default == option.value)?'selected':''} value="${option.value}">${option.name}</option>`;
                        });
                        form += '</select>';
                        break;
                    case 'checkbox':
                        form += `<input type="checkbox" name="${field.name}" ${field.checked?'checked="true"':''}>`;
                        break;
                    
                }
                form += '</div>';
            });
            
            $modal.innerHTML = (
                `<div id="${id}">
                    <h3>${title?title:'please fill in'}</h3>
                    ${form}
                    <button name="validate" id="modal-validate">validate</button>
                    <button name="cancel" id="modal-cancel">cancel</button>
                </div>`);
            
            document.body.classList.add('prompted');
            const $validate = $modal.querySelector('#modal-validate');
            const $cancel = $modal.querySelector('#modal-cancel');

           
            $scene.append($modal);
            
            if(focus != null){
               
                const $fieldFocus = $modal.querySelector(`*[name="${focus}"]`);
                console.log('focus', $fieldFocus);
                if($fieldFocus != null)
                    $fieldFocus.focus();
            }

            $modal.addEventListener('click', e => {
                
                if(e.target == $modal){
                    e.stopPropagation();
                    $cancel.click();
                }
            });

            $validate.addEventListener('click', e => {
                e.stopPropagation();
                const output = {};
                $modal.parentElement.removeChild($modal);
                fields.forEach(field => {
                    const $field = $modal.querySelector(`*[name="${field.name}"]`);
                    output[field.name] = (field.type == 'checkbox')?$field.checked:$field.value;
                });
                resolve(output);
            });

            $cancel.addEventListener('click', e => {
                e.stopPropagation();
                $modal.parentElement.removeChild($modal);
                reject('cancelled');
            });



        });
    };
    
    const displayInfo = (message, type) => {
    
        $infoDisplay.innerText = message;
        $infoDisplay.classList.add(type, 'show');
        setTimeout(()=>{
            $infoDisplay.classList.remove(type, 'show');
        }, 5000);
    };


    



    return {init, toggleSpaceArrows, addSpace, displayInfo, updateView,
        nodeForm, edgeForm, viewForm, modalForm, createNode, registerNode, removeNode, createEdge, registerEdge, removeEdge, upEdge,
        updateLinkedEdges, disableLinkedEdges, enableLinkedEdges, selectNode, unselectNode, unselectNodes, changeLinkedNodes,
        get selectedNodes(){return selectedNodes},
        get nodeTypes(){return dataNodeTypes},
        get edgeTypes(){return dataEdgeTypes},
        get tools(){return tools},
        get $spaceArrows(){return $spaceArrows},
        get $viewButton(){return $viewButton},
        get $scene(){return $scene},
        get edgesByNodeId(){return edgesByNodeId}
    };

})();

export default Interface;
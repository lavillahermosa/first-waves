<?php namespace ProcessWire;

if(!defined("PROCESSWIRE")) die();


/**
 * TRANSLATABLE STRINGS
 * Include translatable strings
 * @see /site/translations.php
 * @var $ferry
 */
$this->wire('tr', '/site/translate.php', true);

/** @var ProcessWire $wire */

/**
 * ProcessWire Bootstrap API Ready
 * ===============================
 * This ready.php file is called during ProcessWire bootstrap initialization process.
 * This occurs after the current page has been determined and the API is fully ready
 * to use, but before the current page has started rendering. This file receives a
 * copy of all ProcessWire API variables.
 *
 */
